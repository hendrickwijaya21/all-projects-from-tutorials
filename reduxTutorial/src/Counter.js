import React from 'react';
import { Text, View, StyleSheet, Button } from 'react-native';
import {useSelector, useDispatch} from 'react-redux'

import {addition, substraction} from './store/action'

const Counter = () => { 
  const data = useSelector(state=>state.counter);
  const dispatch = useDispatch();
  return (
    <View style = {styles.container}>
      <Button title="Add" onPress={()=> {
        dispatch(addition);
      }}/>
      <Text>{data}</Text>
      <Button title="Substract" onPress={()=> {
        dispatch(substraction);
      }}/>
    </View>
);
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  }
})

export default Counter;
