/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {store} from './src/store/store'
import Counter from './src/Counter'

const App: () => React$Node = () => {
  return (
    <Provide store = {store}>
      <Counter/>
    </Provide>
  );
};

const styles = StyleSheet.create({
});

export default App;
