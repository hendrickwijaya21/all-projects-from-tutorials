# Simple User Registration with PHP and MySQL in React Native

## Context and Goal
Learn how to integrate React Native with database that uses PHP and MySQL to get some familiarity with this integration. Learn it from this [video](https://www.youtube.com/watch?v=xo_ZRD_IAp0&feature=youtu.be)

### Note: Put the insert.php in the htdocs/api folder for the database to be functional
