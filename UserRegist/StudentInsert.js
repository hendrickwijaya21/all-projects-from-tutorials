import React, { Component } from 'react';
import { View, Text, TextInput, StyleSheet, Button } from 'react-native';

export default class StudentInsert extends Component {
  constructor(props) {
    super(props);
    this.state = {
      RolNo:'',
      StudentName:'',
      Course:''
    };
  }

  InsertRecord=() => {
    var RolNo=this.state.RolNo;
    var StudentName= this.state.StudentName;
    var Course = this.state.Course;

    if(RolNo.length == 0 || StudentName.length == 0 || Course.length == 0){
      alert('Require Field is Missing');
    } else{
      // Fetch API;
      var InsertAPIURL = 'http://10.0.2.2:80/api/insert.php';

      var headers={
        'Accept':'application/json',
        'Content-Type':'application/json'
      };

      var Data = {
        RolNo,
        StudentName,
        Course
      }
      fetch(InsertAPIURL,{
        method:'POST',
        headers,
        body:JSON.stringify(Data)
      })
      .then((res)=>res.json())
      .then((res)=>{
        alert(res[0].Message);
      })
      .catch((err)=>{
        alert('Error', err)
      })
    }
  }

  render() {
    return (
      <View style={Styles.ViewStyle}>
        <TextInput
          placeholder={'RollNo'}
          placeholderTextColor={'#FF0000'}
          keyboardType={'numeric'}
          style={Styles.textStyle}
          onChangeText={RolNo=>this.setState({RolNo})}
        />
        <TextInput
          placeholder={'Student Name'}
          placeholderTextColor={'#FF0000'}
          style={Styles.textStyle}
          onChangeText={StudentName=>this.setState({StudentName})}
        />
        <TextInput
          placeholder={'Course'}
          placeholderTextColor={'#FF0000'}
          style={Styles.textStyle}
          onChangeText={Course=>this.setState({Course})}
        />
        <Button
          title={'Save Record'}
          onPress={this.InsertRecord}
        />
      </View>
    );
  }
}

const Styles= StyleSheet.create({
  ViewStyle:{
    flex: 1,
    padding: 20,
    marginTop: 10,
  }, 
  textStyle:{
    borderBottomWidth: 1,
    borderBottomColor:'red',
    marginBottom: 20,
  }
});